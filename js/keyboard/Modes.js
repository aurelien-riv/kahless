/* jshint esnext:true */
const Modes = {
    "CAPSLOCK": { "layout": "std", "jsTransform": "toUpperCase", "cssTransform": "uppercase"  },
    "UPPER":    { "layout": "std", "jsTransform": "capitalize" , "cssTransform": "capitalize" },
    "LOWER":    { "layout": "std", "jsTransform": "toLowerCase", "cssTransform": "lowercase"  },
    "ALT"  :    { "layout": "alt" }
};