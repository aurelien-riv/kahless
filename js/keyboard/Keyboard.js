/* jshint esnext:true */
function KeyBoard(ui) {
    this.keys = [];
    this.ui = ui;
    this.context = null;

    for (var key of layout.keys) {
        if ("class" in key)
            this.keys.push (new window[key["class"]](key, this));
        else
            this.keys.push (new Key(key, this));
    }
    this.setMode(Modes.LOWER);

    window.addEventListener('resize', () => {
        window.resizeTo(window.innerWidth, this.ui.clientHeight);
    });
    window.navigator.mozInputMethod.oninputcontextchange = () => {
        this.context = navigator.mozInputMethod.inputcontext;
        window.dispatchEvent(new Event('resize'));
    };
}

KeyBoard.prototype = {
    sendCtl: function (code) {
        if (this.context)
            this.context.sendKey(code, 0, 0);
    },
    sendKey: function (code) {
        if (this.context)
            this.context.sendKey(0, code, 0);
    },
    sendKeys: function (string, caseSensitive) {
        if (!caseSensitive && "jsTransform" in this.mode)
            string = string[this.mode.jsTransform]();
        for (var i=0; i < string.length; ++i)
            this.sendKey(string.charCodeAt(i));
        if (this.mode == Modes.UPPER)
            this.setMode (Modes.LOWER);
    },
    setMode: function (mode) {
        this.mode = mode;
        for(var item of document.getElementsByClassName("subkey")) {
            if (item.classList.contains(mode.layout)) {
                item.style.visibility = 'visible';
                if ("cssTransform" in mode)
                    item.style.textTransform = mode.cssTransform;
            } else
                item.style.visibility = 'collapse';
        }
    }
};
