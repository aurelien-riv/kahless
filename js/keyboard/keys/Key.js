/* jshint esnext:true */
function Key (layout, kb) {
    this.layout = layout;
    this.kb = kb;
    this.gesture = null;
    this.timer = null;

    this.ui = document.createElement("div");
    this.ui.this = this;
    this.ui.setAttribute("class", "key");
    var klass, item;
    for (klass of ['std', 'alt'])
        if (klass in this.layout)
            for (item of this.layout[klass])
                this.addKey ((item instanceof Object ? item.show : item), klass);
    this.kb.ui.appendChild(this.ui);

    this.ui.addEventListener("touchstart", this.onTouchStart);
    this.ui.addEventListener("touchmove",  this.onTouchMove);
    this.ui.addEventListener("touchend",   this.onTouchEnd);
}

Key.prototype = {
    addKey: function (character, klass) {
        var skey = document.createElement("div");
            skey.setAttribute("class", "subkey " + klass);
            skey.appendChild(document.createTextNode(character));
        this.ui.appendChild(skey);
    },
    sendKeys: function () {
        var key = this.layout[this.kb.mode.layout][this.gesture.getDirection()];
        if (! (key instanceof Object))
            this.kb.sendKeys (key);
        else if (key.write)
            this.kb.sendKeys (key.write, key.caseSensitive);
        else if (key.ctl)
            this.kb.sendCtl ((typeof(key.ctl) === "string" ? key.ctl.charCodeAt(0) : key.ctl));
    },
    getRepeatDelay: function (d) {
        return (350 * Math.exp(-0.02 * d) + 25);
    },
    onTouchStart: function (ev) {
        var self = this.this;
        self.gesture = new Gesture(ev.changedTouches[0].pageX, ev.changedTouches[0].pageY);
        self.timer = window.setTimeout(self.onTouchHold, self.getRepeatDelay(0), self);
    },
    onTouchMove: function (ev) {
        this.this.gesture.setDest(ev.changedTouches[0].pageX, ev.changedTouches[0].pageY);
    },
    onTouchEnd: function (ev) {
        var self = this.this;
        clearTimeout(self.timer);
        if (! self.gesture.hold)
            self.sendKeys ();
    },
    onTouchHold: function (self) {
        self.gesture.hold = true;
        self.timer = window.setTimeout(self.onTouchHold, self.getRepeatDelay(self.gesture.getLength()), self);
        self.sendKeys ();
    }
};
