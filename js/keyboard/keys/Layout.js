/* jshint esnext:true */
function Layout (layout, kb) {
    Key.call(this, layout, kb);
}

Layout.prototype = Object.create(Key.prototype, {
    constructor: Layout,
    sendKeys: {
        value: function () {
            var mode = this.kb.mode;
            var dir  = this.gesture.getDirection();
            
            if (mode !== Modes.ALT || dir === Directions.C || dir === Directions.SE) {
                if (! this.gesture.hold) {
                    if (dir === Directions.SE)
                        navigator.mozInputMethod.mgmt.next();
                    else {
                        if (dir === Directions.C)
                            mode = (this.kb.mode == Modes.ALT ? Modes.LOWER : Modes.ALT);
                        else if (dir === Directions.S)
                            mode = Modes.LOWER;
                        else if (dir === Directions.N)
                            mode = Modes.UPPER;
                        else if (dir === Directions.NE)
                            mode = Modes.CAPSLOCK;
                        this.kb.setMode (mode);
                    }
                }
            } else
                Key.prototype.sendKeys.call(this);
        }
    }
});
