/* jshint esnext:true */
const Directions = {
    NW: 0,    N:  1,    NE: 2,
    W:  3,    C:  4,    E:  5,
    SW: 6,    S:  7,    SE: 8
};

function Gesture (orig_x, orig_y) {
    this.orig = [orig_x, orig_y];
    this.dx = 0;
    this.dy = 0;
    this.hold = false;
}

Gesture.prototype = {
    setDest: function (dest_x, dest_y) {
        this.dx = this.orig[1] - dest_y;
        this.dy = dest_x - this.orig[0];
    },
    getLength: function () {
        return Math.sqrt(this.dx*this.dx + this.dy*this.dy);
    },
    getAngle: function () {
        return Math.atan2(this.dx, this.dy);
    },
    getDirection: function () {
        const PI_8 = Math.PI / 8;
        
        if (this.getLength() < 5)
            return Directions.C;

        var angle = this.getAngle();
        if (angle > 0) {
            if (angle <  1 * PI_8) return Directions.E;
            if (angle <  3 * PI_8) return Directions.NE;
            if (angle <  5 * PI_8) return Directions.N;
            if (angle <  7 * PI_8) return Directions.NW;
            return Directions.W;
        } else {
            if (angle > -1 * PI_8) return Directions.E;
            if (angle > -3 * PI_8) return Directions.SE;
            if (angle > -5 * PI_8) return Directions.S;
            if (angle > -7 * PI_8) return Directions.SW;
            return Directions.W;
        }
    }
};